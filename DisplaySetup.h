#ifndef DISPLAY_SETUP_H__
#define DISPLAY_SETUP_H__

#include <LiquidCrystal.h>
#include "Arduino.h"

void setupDisplay(LiquidCrystal lcd);

#endif
