#ifndef TIMELAPSE_H__
#define TIMELAPSE_H__

#include <Servo.h>
#include "Arduino.h"
#include "MainMenu.h"
#include "Display.h"
#include "Keypad.h"

typedef void (* servoPosCallback)(float);

void changeServo1Position(float position);
void changeServo2Position(float position);

class Timelapse 
{
private:
  Display & display;
  Keypad & keypad;

  void InternalRun(unsigned long millisRequired, bool simulation);
  void DisplayProgress(int secondsLeft, float progress);
  void PositionServos(float progress);
  void PositionServo(float progress, int servoFunction, int servoStart, int servoEnd, servoPosCallback changePos);

public:
  Timelapse(Display & display, Keypad & keypad, int servo1Pin, int servo2Pin);
  void Simulate();
  void Start();
};

#endif
