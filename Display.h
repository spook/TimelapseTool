#ifndef DISPLAY_H__
#define DISPLAY_H__

#include <Arduino.h>
#include <LiquidCrystal.h>

struct Display
{
  LiquidCrystal & lcd;
  unsigned char cols, rows;

  Display(LiquidCrystal & lcd, unsigned char cols, unsigned char rows);
  void SetCursor(unsigned char col, unsigned char row);
  void Print(unsigned char col, unsigned char row, char * text);
  void Print(char * text);
  void Clear();
  void SetBacklight(bool on);
};

#endif
