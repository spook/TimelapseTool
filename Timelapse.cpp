#include "Timelapse.h"

void Timelapse::DisplayProgress(int secondsLeft, float progress)
{
  int displayProgress = (int)(progress * 100);

  int displaySeconds = secondsLeft % 60;
  secondsLeft /= 60;
  int displayMinutes = secondsLeft % 60;
  secondsLeft /= 60;
  int displayHours = secondsLeft;

  char * line = new char[display.cols + 1];
  memset(line, 32, display.cols);
  line[display.cols] = 0;

  line[0] = displayHours / 10 + 48;
  line[1] = displayHours % 10 + 48;
  line[2] = ':';
  line[3] = displayMinutes / 10 + 48;
  line[4] = displayMinutes % 10 + 48;
  line[5] = ':';
  line[6] = displaySeconds / 10 + 48;
  line[7] = displaySeconds % 10 + 48;

  line[9] = '(';
  line[10] = displayProgress < 100 ? ' ' : ((displayProgress / 100) % 10 + 48);
  line[11] = displayProgress < 10 ? ' ' : ((displayProgress / 10) % 10 + 48);
  line[12] = displayProgress % 10 + 48;
  line[13] = '%';
  line[14] = ')';

  display.Print(0, 1, line);

  delete line;  
}

void Timelapse::PositionServo(float progress, int servoFunction, int servoStart, int servoEnd, servoPosCallback changePos)
{
  float servoValue;
  if (servoFunction == 0)
    servoValue = progress;
  else if (servoFunction == 1)
    servoValue = pow(progress, 2.0);
  else
    servoValue = pow(progress, 0.5);

  float value = (servoStart + (servoEnd - servoStart) * servoValue);
  changePos(value);  
}

void Timelapse::PositionServos(float progress)
{
  PositionServo(progress, servo1Function.GetValue(), servo1Start.GetValue(), servo1End.GetValue(), changeServo1Position);  
  PositionServo(progress, servo2Function.GetValue(), servo2Start.GetValue(), servo2End.GetValue(), changeServo2Position);  
}

Timelapse::Timelapse(Display & display, Keypad & keypad, int servo1Pin, int servo2Pin)
  : display(display), keypad(keypad)
{

}

void Timelapse::InternalRun(unsigned long millisRequired, bool simulation)
{
  display.Clear();
  if (simulation)
  {
    display.Print(0, 0, "Symulacja");
  }
  else
  {
    display.Print(0, 0, "Timelapse");
  }

  unsigned long start = millis();

  bool finished = false;
  while (!finished)
  {
    unsigned long millisElapsed = millis() - start;
    finished = millisElapsed >= millisRequired || keypad.ReadKey() == KEY_SELECT;

    float progress = (float)millisElapsed / (float)millisRequired;
    if (progress > 1.0f)
      progress = 1.0f;      

    PositionServos(progress);    
    DisplayProgress((millisRequired - millisElapsed) / 1000, progress);
  }
}

void Timelapse::Simulate()
{
  InternalRun(simulationPeriod.GetValue() * 1000L, true);
}

void Timelapse::Start()
{
  InternalRun(timelapsePeriod.GetValue() * 1000L, false);
}

