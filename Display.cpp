#include "Display.h"

Display::Display(LiquidCrystal & lcd, unsigned char cols, unsigned char rows)
  : lcd(lcd)
{
  this->cols = cols;
  this->rows = rows;
  lcd.begin(cols, rows);
}

void Display::SetCursor(unsigned char col, unsigned char row)
{
  this->lcd.setCursor(col, row);
}

void Display::Print(unsigned char col, unsigned char row, char * text)
{
  this->lcd.setCursor(col, row);
  this->lcd.print(text);
}

void Display::Print(char * text)
{
  this->lcd.print(text);
}

void Display::Clear()
{
  this->lcd.clear();
}

void Display::SetBacklight(bool on)
{
  if (on)
  {
    pinMode(10, INPUT);
  }
  else
  {
    digitalWrite(10, LOW);
    pinMode(10, OUTPUT);
  }
}

