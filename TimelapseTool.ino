#include "Display.h"
#include "Keypad.h"
#include "Menu.h"
#include "MainMenu.h"
#include "DisplaySetup.h"
#include "Timelapse.h"

#define SERVO_MIN_MS 550
#define SERVO_MAX_MS 2450

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
Display display(lcd, 16, 2);
Keypad keypad;
MainMenu mainMenu(display, mainItems, mainItemCount);
Timelapse timelapse(display, keypad, 2, 3);

Servo servo1, servo2;

void changeServo1Position(float position)
{
  servo1.writeMicroseconds(SERVO_MIN_MS + (position / 180.0f) * (SERVO_MAX_MS - SERVO_MIN_MS));
}

void changeServo2Position(float position)
{
  servo2.writeMicroseconds(SERVO_MIN_MS + (position / 180.0f) * (SERVO_MAX_MS - SERVO_MIN_MS));
}

void setup()
{
  setupDisplay(lcd);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  servo1.attach(2);
  servo2.attach(3);
  
  mainMenu.Show();
}
 
void loop()
{
  int key = keypad.ReadKey();
  int result = mainMenu.Action(key);

  if (result != -1) 
  {
    if (result == ACTION_SIMULATE) {
      timelapse.Simulate();
    } else if (result == ACTION_START) {
      timelapse.Start();
    }

    delay(250);
    mainMenu.Show();
  }
}
