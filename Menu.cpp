#include "Menu.h"

char * NewEmptyLine(int width, bool selected = false)
{
  char * line = new char[width + 1];
    
  memset(line, 32, width);
  line[width] = 0;
  if (selected)
    line[0] = SELECTION_CHAR;
  return line;  
}

void DeleteLine(char * line)
{
  delete line;
}

// BaseMenuItem ---------------------------------------------------------------

void BaseMenuItem::ShowListItem(Display & display, int row, bool selected)
{
  char * line = NewEmptyLine(display.cols, selected);
  
  strncpy(line + 2, this->name, min(display.cols - 2, this->len));
  display.Print(0, row, line);

  DeleteLine(line);
}

BaseMenuItem::BaseMenuItem(const char * name)
{
  this->name = name;
  this->len = strlen(this->name);
}

const char * BaseMenuItem::GetName()
{
  return this->name;
}

// MenuItem -------------------------------------------------------------------

void MenuItem::ShowEditHeader(Display & display)
{
  char * line = NewEmptyLine(display.cols);

  strncpy(line, this->name, min(display.cols, this->len));
  display.Print(0, 0, line);  

  DeleteLine(line);
}

void MenuItem::ShowEditMode(Display & display)
{
  this->ShowEditHeader(display);
  this->ShowEditValue(display);
}

MenuItem::MenuItem(const char * name) : BaseMenuItem(name)
{  

}

// IntMenuItem ----------------------------------------------------------------

IntMenuItem::IntMenuItem(const char * name, int min, int max, int defaultValue)
  : MenuItem(name)
{
  this->min = min;
  this->max = max;
  this->value = defaultValue < this->min ? this->min : (defaultValue > this->max ? this->max : defaultValue);
  this->callback = NULL;
}

IntMenuItem::IntMenuItem(const char * name, int min, int max, int defaultValue, IntValueChangedCallback callback)
  : MenuItem(name)
{
  this->min = min;
  this->max = max;
  this->value = defaultValue < this->min ? this->min : (defaultValue > this->max ? this->max : defaultValue);
  this->callback = callback;
}

void IntMenuItem::StartEdit()
{
  if (callback != NULL)
    (*callback)(this->value);
}

void IntMenuItem::ShowEditValue(Display & display)
{
  char * line = NewEmptyLine(display.cols);
  
  line[0] = 127;
  line[display.cols - 1] = 126;

  int i = display.cols - 3;
  int temp = abs(this->value);
  do
  {
    line[i] = temp % 10 + 48;
    temp /= 10;
    i--;
  }
  while (temp > 0);

  if (this->value < 0)
    line[i] = '-';

  display.Print(0, 1, line);

  DeleteLine(line);
}

int IntMenuItem::GetType() 
{
  return MENU_TYPE_INT;  
}

int IntMenuItem::GetValue()
{
  return this->value;
}

void IntMenuItem::HandleAction(int key)
{
  int change = 0;
  switch (key) {
    case MENU_ACTION_LEFT:
      change = -1;
      break;
    case MENU_ACTION_RIGHT:
      change = 1;
      break;
    case MENU_ACTION_UP:
      change = 10;
      break;
    case MENU_ACTION_DOWN:
      change = -10;
      break;
  }

  if (this->value + change >= this->min && this->value + change <= this->max) {

    this->value = this->value + change;
    if (callback != NULL)
      (*callback)(this->value);
  }
}

// EnumMenuItem ---------------------------------------------------------------

void EnumMenuItem::ShowEditValue(Display & display)
{
  char * line = NewEmptyLine(display.cols);
  line[0] = 127;
  line[display.cols - 1] = 126;

  int itemLen = strlen(items[value]);
  int charsToCopy = min(display.cols - 4, itemLen);
  strncpy(line + 2, items[value], charsToCopy);

  display.Print(0, 1, line);

  DeleteLine(line);
}

EnumMenuItem::EnumMenuItem(const char * name, char ** items, int itemCount, int defaultValue)
  : IntMenuItem(name, 0, itemCount - 1, defaultValue)
{
  this->items = items;
  this->itemCount = itemCount;
}

EnumMenuItem::EnumMenuItem(const char * name, char ** items, int itemCount, int defaultValue, IntValueChangedCallback callback)
  : IntMenuItem(name, 0, itemCount - 1, defaultValue, callback)
{
  this->items = items;
  this->itemCount = itemCount;
}


// TimeMenuItem ---------------------------------------------------------------

void TimeMenuItem::ShowEditValue(Display & display)
{
  char * line = NewEmptyLine(display.cols);
  
  line[0] = 127;
  line[display.cols - 1] = 126;

  int sec = abs(value) % 60;

  int i = display.cols - 3;
  line[i--] = sec % 10 + 48;
  line[i--] = sec / 10 + 48;
  line[i--] = ':';
  
  int mins = abs(value) / 60;
  do
  {
    line[i] = mins % 10 + 48;
    mins /= 10;
    i--;
  }
  while (mins > 0);

  if (this->value < 0)
    line[i] = '-';

  display.Print(0, 1, line);

  DeleteLine(line);
}

TimeMenuItem::TimeMenuItem(const char * name, int min, int max, int defaultValue)
  : IntMenuItem(name, min, max, defaultValue)
{
  
}

TimeMenuItem::TimeMenuItem(const char * name, int min, int max, int defaultValue, IntValueChangedCallback callback)
  : IntMenuItem(name, min, max, defaultValue, callback)
{
  
}

// BoolMenuItem ---------------------------------------------------------------

BoolMenuItem::BoolMenuItem(const char * name, bool defaultValue)
  : MenuItem(name)
{
  this->value = defaultValue;    
  this->callback = NULL;
}

BoolMenuItem::BoolMenuItem(const char * name, bool defaultValue, BoolValueChangedCallback callback)
 : MenuItem(name)
{
  this->value = defaultValue;
  this->callback = callback;
}

void BoolMenuItem::StartEdit()
{
  if (callback != NULL)
    (*callback)(this->value);
}

void BoolMenuItem::ShowEditValue(Display & display)
{
  char * line = NewEmptyLine(display.cols);
  
  line[0] = 127;
  line[display.cols - 1] = 126;

  if (this->value)
  {
    strncpy(line + display.cols - 4, "On", 2);    
  }
  else
  {
    strncpy(line + display.cols - 5, "Off", 3);
  }

  display.Print(0, 1, line);

  DeleteLine(line);
}

int BoolMenuItem::GetType() 
{
  return MENU_TYPE_BOOL;
}

bool BoolMenuItem::GetValue()
{
  return this->value;
}

void BoolMenuItem::HandleAction(int key)
{
  if (key == MENU_ACTION_LEFT || key == MENU_ACTION_RIGHT || key == MENU_ACTION_UP || key == MENU_ACTION_DOWN) 
  {
    value = !value;
    if (callback != NULL)
      (*callback)(value);
  }
}

// SubMenuItem ----------------------------------------------------------------

SubMenuItem::SubMenuItem(const char * name, BaseMenuItem ** items, int count) : BaseMenuItem(name)
{
  this->items = items;
  this->count = count;
}

int SubMenuItem::GetCount()
{
  return this->count;
}

BaseMenuItem * SubMenuItem::GetItem(int index)
{
  return this->items[index];
}

int SubMenuItem::GetType() 
{
  return MENU_TYPE_SUB;
}

// ActionMenuItem -------------------------------------------------------------

ActionMenuItem::ActionMenuItem(const char * name, int actionId) : BaseMenuItem(name)
{
  this->actionId = actionId;
}

int ActionMenuItem::GetActionId()
{
  return this->actionId;
}

int ActionMenuItem::GetType() 
{
  return MENU_TYPE_ACTION;  
}

// MainMenu -------------------------------------------------------------------

void MainMenu::ShowBack(int row, bool isSelected)
{
  char * line = NewEmptyLine(display.cols, isSelected);

  strncpy(line + 2, TEXT_BACK, TEXT_BACK_LEN);
  display.Print(0, row, line);

  DeleteLine(line);
}

void MainMenu::Show()
{  
  if (editedItem == NULL)
  {
    bool isSubMenu = stackTop > 0;

    for (int i = 0; i < display.rows; i++)
    {
      if (isSubMenu && i + firstItem == itemStack[stackTop]->GetCount())
      {
        ShowBack(i, i + firstItem == selectedItem);
      }
      else if (i + firstItem < itemStack[stackTop]->GetCount())
      {
        itemStack[stackTop]->GetItem(i + firstItem)->ShowListItem(display, i, i + firstItem == selectedItem);
      }
      else
      {
        char * line = NewEmptyLine(display.cols);
        display.Print(0, i, line);
        DeleteLine(line);
      }
    }
  }
  else
  {
    editedItem->ShowEditMode(display);
  }
}

MainMenu::MainMenu(Display & display, BaseMenuItem ** items, int count) : 
  SubMenuItem(NULL, items, count),
  display(display)
{
  // Assuming initially 5 levels of menu max
  itemStack = new SubMenuItem * [MAX_MENU_LEVELS];
  positionStack = new short[MAX_MENU_LEVELS * 2];
  stackTop = 0;
  itemStack[stackTop] = this;
}

MainMenu::~MainMenu()
{
  delete itemStack;
  delete positionStack;
}

int MainMenu::Action(int keyAction)
{
  int lastItemId = stackTop > 0 ? itemStack[stackTop]->GetCount() : itemStack[stackTop]->GetCount() - 1;

  if (editedItem == NULL)
  {
    if (keyAction == MENU_ACTION_UP || keyAction == MENU_ACTION_DOWN) 
    {      
      if (keyAction == MENU_ACTION_UP)
        this->selectedItem--;
      else
        this->selectedItem++;
          
      if (selectedItem < 0)
        selectedItem = lastItemId;      
      if (selectedItem > lastItemId)
        selectedItem = 0;    
  
      // Adjust items in view
      
      while (firstItem > selectedItem)
        firstItem--;
      while (firstItem + display.rows - 1 < selectedItem)
        firstItem++;
  
      Show();
      return -1;
    }
    else if (keyAction == MENU_ACTION_SELECT) 
    {
      if (selectedItem == lastItemId && stackTop > 0) 
      {
        // Chosen Back
        
        stackTop--;
        selectedItem = positionStack[stackTop * 2];
        firstItem = positionStack[stackTop * 2 + 1];
  
        Show();
      }
      else
      {
        int type = itemStack[stackTop]->GetItem(selectedItem)->GetType();
        switch (type)
        {
          case MENU_TYPE_INT:
          {
            editedItem = (MenuItem *)itemStack[stackTop]->GetItem(selectedItem);
            editedItem->StartEdit();
            Show();
            break;
          }
          case MENU_TYPE_BOOL:
          {
            editedItem = (MenuItem *)itemStack[stackTop]->GetItem(selectedItem);
            editedItem->StartEdit();
            Show();
            break;
          }
          case MENU_TYPE_SUB:
          {            
            if (stackTop >= MAX_MENU_LEVELS - 1)
              return -1;
      
            positionStack[stackTop * 2] = selectedItem;
            positionStack[stackTop * 2 + 1] = firstItem; 
  
            itemStack[stackTop + 1] = ((SubMenuItem *)(itemStack[stackTop]->GetItem(selectedItem)));
            stackTop++;
            selectedItem = 0;
            firstItem = 0;
  
            Show();
            
            break;
          }
          case MENU_TYPE_ACTION:
          {
            int actionId = ((ActionMenuItem *)(itemStack[stackTop]->GetItem(selectedItem)))->GetActionId();
            return actionId;
          }
        }
      }
    }
  }
  else
  {
    if (keyAction == MENU_ACTION_LEFT || keyAction == MENU_ACTION_RIGHT || keyAction == MENU_ACTION_UP || keyAction == MENU_ACTION_DOWN) 
    {
      editedItem->HandleAction(keyAction);  
      Show();
    }
    else if (keyAction == MENU_ACTION_SELECT)
    {
      editedItem = NULL;
      Show();
    }
  }
  return -1;
}

