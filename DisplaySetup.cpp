#include "DisplaySetup.h"

void setupDisplay(LiquidCrystal lcd)
{
  byte settings[8] = {
    B10010,
    B10010,
    B01100,
    B01100,
    B01100,
    B10010,
    B10010,
    B00000,
  };

 byte simulation[8] = {
    B10000,
    B11000,
    B10100,
    B10010,
    B10100,
    B11000,
    B10000,
    B00000
 };

 byte start[8] = {
    B10000,
    B11000,
    B11100,
    B11110,
    B11100,
    B11000,
    B10000,
    B00000
 };

 byte linear[8] = {
    B00000,
    B00001,
    B00010,
    B00100,
    B01000,
    B10000, 
    B00000,
    B00000
 };

 byte square[8] = {
    B00000,
    B00001,
    B00001,
    B00001,
    B00010,
    B11100,
    B00000,
    B00000
 };

 byte invSquare[8] = {
    B00000,
    B00111,
    B01000,
    B10000,
    B10000,
    B10000,
    B00000,  
    B00000
 };

  lcd.createChar(1, settings);
  lcd.createChar(2, simulation);
  lcd.createChar(3, start);
  lcd.createChar(4, linear);
  lcd.createChar(5, square);
  lcd.createChar(6, invSquare);
}

