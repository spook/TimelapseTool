#ifndef MAIN_MENU_H__
#define MAIN_MENU_H__

#include "Menu.h"

#define ACTION_SIMULATE 1
#define ACTION_START 2

extern char * functionItems[];
    
extern IntMenuItem servo1Start;
extern IntMenuItem servo1End;
extern EnumMenuItem servo1Function;
  
extern IntMenuItem servo2Start;
extern IntMenuItem servo2End;
extern EnumMenuItem servo2Function;
  
extern BaseMenuItem * servo1Items[];  
extern BaseMenuItem * servo2Items[];
  
extern TimeMenuItem simulationPeriod;
extern TimeMenuItem timelapsePeriod;
extern BoolMenuItem disableBacklight;
  
extern BaseMenuItem * settingItems[];
extern BaseMenuItem * mainItems[];
extern int mainItemCount;
#endif
