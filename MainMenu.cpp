#include "MainMenu.h"

char * functionItems[] = {
    "\x04 Liniowa",
    "\x05 Kwadr.",
    "\x06 Odw. kwadr."
  };

void changeServo1Position(float position);
void changeServo2Position(float position);

void setServo1Position(int position) {
  changeServo1Position(position);
}

void setServo2Position(int position) {
  changeServo2Position(position);
}

IntMenuItem servo1Start("Poz. startowa", 0, 180, 0, setServo1Position);
IntMenuItem servo1End("Poz. koncowa", 0, 180, 180, setServo1Position);
EnumMenuItem servo1Function("Funkcja", functionItems, sizeof(functionItems) / sizeof(char *), 0);

IntMenuItem servo2Start("Poz. startowa", 0, 180, 90, setServo2Position);
IntMenuItem servo2End("Poz. koncowa", 0, 180, 90, setServo2Position);
EnumMenuItem servo2Function("Funkcja", functionItems, sizeof(functionItems) / sizeof(char *), 0);

BaseMenuItem * servo1Items[] =
{
  &(servo1Start),
  &(servo1End),
  &(servo1Function),
};

BaseMenuItem * servo2Items[] =
{
  &(servo2Start),
  &(servo2End),
  &(servo2Function)
};

TimeMenuItem simulationPeriod("Symul. (mm:ss)", 0, 60, 20);
TimeMenuItem timelapsePeriod("Czas (mm:ss)", 0, 3600, 1200);
BoolMenuItem disableBacklight("Wylacz pods.", false);

BaseMenuItem * settingItems[] =
{
  new SubMenuItem("Serwo 1", servo1Items, sizeof(servo1Items) / sizeof(BaseMenuItem *)),
  new SubMenuItem("Serwo 2", servo2Items, sizeof(servo2Items) / sizeof(BaseMenuItem *)),
  &(timelapsePeriod),
  &(simulationPeriod),
  &(disableBacklight)
};

BaseMenuItem * mainItems[] = 
{
  new SubMenuItem("\x01 Ustawienia", settingItems, sizeof(settingItems) / sizeof(BaseMenuItem *)),
  new ActionMenuItem("\x02 Symulacja", ACTION_SIMULATE),
  new ActionMenuItem("\x03 Start", ACTION_START)
};

int mainItemCount = sizeof(mainItems) / sizeof(BaseMenuItem *);
